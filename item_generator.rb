require_relative 'item'

class ItemGenerator

	  def initialize(file_content)
	    @file_content=file_content
	  end
    
    def generate_item
    	item_obj=[]
      for i in 1..@file_content.length-1
    	  item_obj << Item.new(@file_content[i])
      end
      return item_obj
	  end
end
