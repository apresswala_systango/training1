class ReadFile
  
  def initialize(path_to_file)
    @path_to_file = path_to_file
  end  

  def read_file
    begin
      require 'csv'
      file=open(@path_to_file,"r")
      file_content=CSV.read(@path_to_file)
    rescue Exception=>e
      puts e
    end
      return file_content   
  end	
  
end  
