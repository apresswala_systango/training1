class Verification

  def check_permission(input_item,data_item)
      permissions_wanted=input_item.get_permission.split(";")
      permissions_granted=data_item.get_permission.split(";")
    
      allowed=permissions_wanted&permissions_granted
      notallowed=permissions_wanted-permissions_granted

      if allowed.empty? 
         puts "user "+input_item.get_username+": dont have permission "+notallowed.to_s
      elsif !allowed.empty? && !notallowed.empty?
         puts "user "+input_item.get_username+": have permission "+allowed.to_s+" : dont have permission "+notallowed.to_s
      else
         puts "user "+input_item.get_username+": allowed"
      end  
  end

  
  def check_username_password(input_item,data_item_object)
      flag="false" 
      for i in 0..data_item_object.length-1 
         if input_item.get_username===data_item_object[i].get_username
            flag1="true"
         end
         if input_item.get_username===data_item_object[i].get_username && input_item.get_password===data_item_object[i].get_password
            check_permission(input_item,data_item_object[i])
            flag="true"
         else
            next        
         end
      end

      if flag1==="true" && flag==="false"
         puts "user "+input_item.get_username+": password mismatch" 
      elsif flag==="false"
         puts "user "+input_item.get_username+": doesnt exist"   
      end  
  end

  def verify_user(data_item_object,input_item_object)
      for i in 0..input_item_object.length-1
          check_username_password(input_item_object[i],data_item_object)
      end
  end

end