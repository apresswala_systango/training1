class Item
    @username
    @password
    @permission

	def initialize(file_content)
		@username=file_content[0]
		@password=file_content[1]
		@permission=file_content[2]
   end

	def to_s
		return @username.to_s + ", " + @password.to_s + ", " + @permission.to_s 
	end

	def get_username
		@username
	end	

	def get_password
		@password
	end	
	
	def get_permission
		@permission
	end	
	
	
	def set_username(username)
		@username=username
	end

	def set_password(password)
		@password=password
	end

	def set_permission(permission)
		@permission=permission
	end
	
end	
